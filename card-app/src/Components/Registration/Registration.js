import React,{useState} from 'react'
import googleImg from '../../Assets/google.svg'
import githubImg from '../../Assets/github.svg'
import facebookImg from '../../Assets/facebook.svg'
import twitterImg from '../../Assets/twitter.svg'
import sideImg from '../../Assets/registration.png'
import '../Registration/Registration.css';
const Registration = (props) => {

    const {loggin1}=props;

    const [fname, setfname] = useState('');
    const [fnameErr, setfnameErr] = useState('');
    const [email, setemail] = useState('');
    const [emailErr, setemailErr] = useState('')
    const [pass, setpass] = useState('');
    const [passErr, setpassErr] = useState('');
    const [commonErr, setcommonErr] = useState('')


    // [a-z0-9]+@[a-z]+\.[a-z]{2,3}
    // ^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})

    const onChangeFname = (e) => {
        const pattern = /^[a-zA-z]{2,10}$/;
        if(pattern.test(e.target.value)){
            setfname(e.target.value);
            setfnameErr('');
        }
        else if(e.target.value===''){
            setfnameErr('*Please, Enter Name');
        }
        else{
            setfnameErr('*Sorry, First name is not valid.');
        }
    }
    const onChangeEmail = (e) => {
        const pattern = /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/;
        if(pattern.test(e.target.value)){
            setemail(e.target.value);
            setemailErr('');
        }
        else if(e.target.value===''){
            setemailErr('*Please, Enter email.');
        }
        else{
            setemailErr('*Sorry, Email is not valid.');
        }
    }
    const onchangePass =(e)=>{
        const pattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
        // const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
        if(pattern.test(e.target.value)){
            setpass(e.target.value);
            setpassErr('');
        }
        else if(e.target.value===''){
            setpassErr('*Please, Enter Password');
        }
        else{
            setpassErr('*Sorry, Password is not valid.');
        }
    }
    const onclickSignUp=(e)=>{
        let tcp=document.getElementById('flexCheckDefault');
        if(fname===''){
            setfnameErr('*Please, Enter Name');
            // setcommonErr('*Please, Enter Name');
        }
        if(email===''){
            setemailErr('*Please, Enter Email');
            // setcommonErr('*Please, Enter Email');
        }
        if(pass===''){
            setpassErr('*Please, Enter Password');
            // setcommonErr('*Please, Enter Password');
        }
        if(tcp.checked!==true){
            setcommonErr('*Please, Agree terms and condition');
        }
        else{
            setcommonErr('');
        }
        e.preventDefault();
    }
    const onClickSignIn=(e)=>{
        loggin1();
        e.preventDefault();
    }
    return (
        <div className="main ">
            <div className="form">
                <div className="container-fluid d-flex justify-content-center my-min-high align-items-center">
                    <div className="row shadow rounded-3">
                        <div className="col-md-5 offset-md-1">
                            <div className="text-center my-2 fs-3 fw-bolder text-success">
                                Create Account
                            </div>
                            <div className="social-icons d-flex justify-content-center my-3">
                                <div className="google-icon mx-2">
                                    <button className="my-btn-bg-remove">
                                        <img src={googleImg} alt="" height="20px" width="20px" />
                                    </button>
                                </div>
                                <div className="facebook-icon mx-2">
                                    <button className="my-btn-bg-remove">
                                        <img src={facebookImg} alt="" height="20px" width="20px" />
                                    </button>
                                </div>
                                <div className="twitter-icon mx-2">
                                    <button className="my-btn-bg-remove">
                                        <img src={twitterImg} alt="" height="20px" width="20px" />
                                    </button>
                                </div>
                                <div className="github-icon mx-2">
                                    <button className="my-btn-bg-remove">
                                        <img src={githubImg} alt="" height="20px" width="20px" />
                                    </button>
                                </div>
                            </div>
                            <div className="noteText text-center mt-4 mb-3 text-muted my-fs1 fw-lighter">
                                Already have an account?  <a href="/" className="my-a fw-normal" onClick={onClickSignIn}> Sign In</a>
                            </div>
                            <form onSubmit={onclickSignUp}>
                                <div className="regfrom">
                                    <div className="name my-1">
                                        <input type="text" className="form-control my-fs1" id="exampleFormControlInput1" placeholder="Name"  onChange={onChangeFname}/>
                                    </div>
                                    <div className="errMes">{fnameErr}</div>
                                    <div className="email my-1">
                                        <input type="email" className="form-control my-fs1" id="exampleFormControlInput2" placeholder="Email" onChange={onChangeEmail}/>
                                    </div>
                                    <div className="errMes">{emailErr}</div>
                                    <div className="password my-1">
                                        <input type="password" className="form-control my-fs1 " id="exampleFormControlInput3" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;"  onChange={onchangePass}/>
                                    </div>
                                    <div className="errMes">{passErr}</div>
                                </div>
                                <div className="tcp">
                                    <div className="">
                                        <input className="form-check-input my-check-hover" type="checkbox" value="" id="flexCheckDefault" />
                                        <label className="ps-2 form-check-label my-fs align-center" >
                                            I agree to the <span className="text-success"> <a href="/" className="my-cutomise-a"> Terms</a></span> and <span className="text-success"> <a href="/" className="my-cutomise-a"> Privacy Policy.</a></span>
                                        </label>
                                    </div>
                                    <div className="errMes">{commonErr}</div>
                                </div>
                                <div className="btn-su-si mb-4 d-flex">
                                    <div className="btn-su w-100 pe-1">
                                        {/* <button type="button" className="btn btn-success btn-su  me-1 w-100" onClick={onclickSignUp}>Sign Up</button> */}
                                        <input type="submit" className="btn btn-success btn-su  me-1 w-100"  value="Sign Up" />
                                    </div>
                                    {/* <div className="btn-su w-50 ps-1">
                                        <button type="button" className="btn btn-outline-success w-100" onClick={onClickSignIn}>Sign In</button>
                                    </div> */}
                                </div>
                            </form>
                        </div>
                        <div className="col-md-6">
                            <img src={sideImg} className="img-fluid h-100" alt="..."/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Registration;
