import React, { useState } from "react";
import Login from "./component/Login/Login";
import Registration from "./component/Registration/Registration";
// import Destructing from './component/Practice/Destructing';
// import Form from './component/Practice/Form';
// import Heading from './component/Practice/Heading';
// import LCmethods from './component/LCmethods';
// import Tike from './component/Practice/Tike';
// import CondiRender from './component/Practice/CondiRender';
// import {LoginButton,LogoutButton} from './component/Practice/UserGuest';
// import RandomCard from './component/CardTask/RandomCard';

function App() {
  // const [isIn, setisIn] = useState(false);
  const [isLoggin, setisLoggin] = useState(true);

  // const user = {
  //   fname:'Alvero',
  //   lname:'Moreno'
  // }

  // const color ={
  //   clr1:'Red',
  //   clr2:'Yellow',
  //   clr3:'Blue',
  //   clr4:'White'
  // }

  // const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  const toggleForm = () => {
    setisLoggin(!isLoggin);
  }
  return (
    <div className="App">
      {/* <Heading userObj={user}/> */}
      {/* <Heading userObj={user}/> */}
      {/* <Tike data={new Date()}/> */}
      {/* <Form/> */}
      {/* <LCmethods fname="Alvero"/> */}
      {/* <Destructing clrObj={color} day={days}/> */}
      {/* <CondiRender isLoggin={false}/> */}
      {/* <RandomCard/> */}
      {
        isLoggin === true ?
          <Login loggin={toggleForm}/> 
        :
          <Registration loggin1={toggleForm}/>
      }
    </div>
  );
}

export default App;
