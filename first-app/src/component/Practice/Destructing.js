import React,{useState} from 'react';

const Destructing = (props) => {
    const{clrObj,day}=props
    const [useObj, setuseObj] = useState({test1:'Test1',test2:'Test2',test3:'Test3'});    
    const [useArr, setuseArr] = useState(['Apple','Banana','Orange']);


    console.log(useObj);

    console.log(clrObj);

    //Props destructuring
    
    console.log(clrObj);
    console.log(day);

    //Object destructuring
    const {clr1,clr3}=clrObj;
    console.log(clr1);
    console.log(clr3);

    //Array destructuring
    const [day1,day2]=day;
    console.log(day1);
    console.log(day2);

    //state obj destructuring
    const {test1,test3}=useObj;
    console.log(test1);
    console.log(test3);

    //state Arr destrucuring
    const [fruit1,fruit2]=useArr;
    console.log(fruit1);
    console.log(fruit2);

    return (
        <div>
            <div>This data is come from Object Destructing</div>
            <div>-----------------------------------------</div>
            <div>First color:{clr1}</div> 
            <div>Third color:{clr3}</div> 
            <div>-----------------------------------------</div>
            <div>This data is come from Array Destructing</div>
            <div>-----------------------------------------</div>
            <div>Array[0]:{day1}</div>
            <div>Array[1]:{day2}</div>
        </div>
    )
}
export default Destructing;
