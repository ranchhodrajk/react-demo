import React from 'react'
const Footer = (props) => {
    const{titleName}=props;
    const currentYear=new Date().getFullYear()
    return (
        <div>
            <nav className=" py-3 navbar-dark bg-dark">
                <div className="container-fluid">
                    <div className=" text-muted text-center">
                        &copy; {currentYear} {titleName} All rights reserved.
                    </div>
                </div>
            </nav>
        </div>
    )
}
export default Footer