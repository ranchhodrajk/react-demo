import React, { useState } from "react";
import Login from "./Components/Login/Login";
import Registration from "./Components/Registration/Registration";
function App() {
  const [isLoggin, setisLoggin] = useState(true);
  const toggleForm = () => {
    setisLoggin(!isLoggin);
  }
  return (
    <div className="App">
      {
        isLoggin === true ?
          <Login loggin={toggleForm}/> 
        :
          <Registration loggin1={toggleForm}/>
      }
    </div>
  );
}

export default App;
