import React, { Component } from 'react';

class Form extends Component {
    constructor(props){
        super(props);
        this.state={
            fname:'',
            lname:'',
            onOff:true
        }
    }
    onchangeFname = (event) =>{
        this.setState({fname:event.target.value});
    }

    onchangeLname = (event) =>{
        this.setState({lname:event.target.value});
    }
    onSubmitShow = (e)=>{
        alert(`Hello, ${this.state.fname} ${this.state.lname} .`)
        e.preventDefault();
    }

    onclickToggle = (e) =>{
        this.setState((state)=>({
            onOff:!state.onOff
        }));
        console.log(this.state.onOff);
    }
    render() {
        return (
            <div>
                <div className="main">
                    <form onSubmit={this.onSubmitShow}>
                        <div className="fname">
                            <div className="fname-txt">Fist Name:</div>
                            <div className="fname-input">
                                <input type="text" name="fname" onChange={this.onchangeFname}/>
                            </div>
                        </div>
                        <div className="lname">
                            <div className="lname-txt">Last Name:</div>
                            <div className="lname-input">
                                <input type="text" name="lname"  onChange={this.onchangeLname}/>
                            </div>
                        </div>
                        <br/>
                        <div className="btn">
                            <input type="submit" name="submit"/>
                            {/* <button name="showBtn" onClick={this.onclickShow}>Click Me!</button> */}
                        </div>
                        <br/>
                        <div className="btn1">
                            <button name="onOff" onClick={this.onclickToggle}>On / Off</button>
                        </div>
                    </form>
                </div>
                <br/>
                <div className="fname-op">Your First Name is: {this.state.fname}</div>
                <div className="fname-op">Your Last Name is: {this.state.lname}</div>
                <div className="onOff">Bulb is: {this.state.onOff?'On':'Off'}</div>

            </div>
            
        )
    }
}

export default Form