console.log("Hello Es6");

console.log("=================================================================");
console.log("Arrow Function");
console.log("=================================================================");

const greet = () => {
    console.log("Hello arrow function.");
}

greet();

console.log("=================================================================");
console.log("Var Let Const");
console.log("=================================================================");

var a = 1;
let b = 2;
const c = 3;
console.log("Outside the block [before]");
console.log("Var a :" + a);
console.log("Let b :" + b);
console.log("Const c :" + c);
{

    let b=4;
    const c=5;

    console.log("Inside the block")
    console.log("Var a :" + a);
    console.log("Let b :" + b);
    console.log("Const c :" + c);
}

// const c=6;     this will give you an error because we cant change const value;
var a=10;  // its overwrite a value

console.log("Outside the block [after]");
console.log("Var a :" + a);
console.log("Let b :" + b);
console.log("Const c :" + c);


console.log("=================================================================");
console.log("Spread and Rest Operators");
console.log("=================================================================");
console.log("============================Spread Operator======================");

console.log("=====With Object=====");
const myObj = {
    fname:"Alvero",
    lname:"Moreno",
};
console.log(myObj);

const updateMyObj = {...myObj,age:"35"};
console.log(updateMyObj);

console.log("=====With array=====");

const numbers = [1,2,3];
console.log(numbers);

const addNumber = [...numbers,4,5];
console.log(addNumber);

console.log("=====With Both=====");

const data = [
    {
        fname:"John",
        lname:"Doe"
    },
    {
        fname:"Licaa",
        lname:"Jhan"
    }
];
console.log(data);

const flname = {
    fname:"Jack",
    lname:"Piterson"
}

const newData = [...data,flname];
console.log(newData);

console.log("=============================Rest Operator=======================");

const rest = (...items) => {
    console.log(items.sort());
}
rest(11,51,20,45,10);

console.log("=================================================================");
console.log("Destructuring")
console.log("=================================================================");
console.log("=====With array=====");

const info = [1,"XYZ",2,"ABC"];
[num1,d1,num2]=info;

console.log(num1);
console.log(d1);
console.log(num2);
// console.log(d2);

console.log("=====With object=====");

const otherObj = {
    xname : "LKJ",
    ph:111,
    gen:"male"
};
const {xname,ph,gen} = otherObj;
console.log(xname)
console.log(gen);
console.log(ph);
console.log("=================================================================");

const infoData = [
        {
            userId: 1,
            id: 1,
            title: "1sunt aut facere ",
            body: "quia et suscipit suscipit recusandae consequuntur expedita et cum \
            reprehenderit molestiae ut ut quas totam \
            nostrum rerum est autem sunt rem eveniet architecto"
        },
        {
        userId: 1,
        id: 2,
        title: "2qui est esse",
        body: "est rerum tempore vitae \
        sequi sint nihil reprehenderit dolor beatae ea dolores neque  \
        fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis \
        qui aperiam non debitis possimus qui neque nisi nulla"
        },
        {
        userId: 1,
        id: 3,
        title: "3ea molestias quasi exercitationem repellat qui ipsa sit aut",
        body: "et iusto sed quo iure \
        voluptatem occaecati omnis eligendi aut ad \
        voluptatem doloribus vel accusantium quis pariatur \
        molestiae porro eius odio et labore et velit aut"
        },
        {
        userId: 1,
        id: 4,
        title: "4eum et est occaecati",
        body: "ullam et saepe reiciendis voluptatem adipisci\
        sit amet autem assumenda provident rerum culpa\
        quis hic commodi nesciunt rem tenetur doloremque ipsam iure\
        quis sunt voluptatem rerum illo velit"
        },
        {
        userId: 1,
        id: 5,
        title: "5nesciunt quas odio",
        body: "repudiandae veniam quaerat sunt sed\
        alias aut fugiat sit autem sed est\
        voluptatem omnis possimus esse voluptatibus quis\
        est aut tenetur dolor neque"
        },
        {
        userId: 1,
        id: 6,
        title: "6dolorem eum magni eos aperiam quia",
        body: "ut aspernatur corporis harum nihil quis provident sequi\
        mollitia nobis aliquid molestiae\
        perspiciatis et ea nemo ab reprehenderit accusantium quas\
        voluptate dolores velit et doloremque molestiae"
        },
        {
        userId: 1,
        id: 7,
        title: "7magnam facilis autem",
        body: "dolore placeat quibusdam ea quo vitae\
        magni quis enim qui quis quo nemo aut saepe\
        quidem repellat excepturi ut quia\
        sunt ut sequi eos ea sed quas"
        },
        {
        userId: 1,
        id: 8,
        title: "8dolorem dolore est ipsam",
        body: "dignissimos aperiam dolorem qui eum\
        facilis quibusdam animi sint suscipit qui sint possimus cum\
        quaerat magni maiores excepturi\
        ipsam ut commodi dolor voluptatum modi aut vitae"
        },
        {
        userId: 1,
        id: 9,
        title: "9nesciunt iure omnis dolorem tempora et accusantium",
        body: "consectetur animi nesciunt iure dolore\
        enim quia ad\
        veniam autem ut quam aut nobis\
        et est aut quod aut provident voluptas autem voluptas"
        },
        {
        userId: 1,
        id: 10,
        title: "10optio molestias id quia eum",
        body: "quo et expedita modi cum officia vel magni\
        doloribus qui repudiandae\
        vero nisi sit\
        quos veniam quod sed accusamus veritatis error"
        }
];

console.log(infoData);

const onClickReducerData = () =>{

    let html = '';

    const op = infoData.reduce((acc,curr)=>{
        let lt = curr.title;
        if(lt.length>30){
            acc = curr.title
        }
        html += `<h1>${acc}</h1>`
        return html;
    },{});
    $('#info').html(op);
}


const onClickAllData = () =>{
    let html = '';
    infoData.map((item)=>{
        html += `
            <h1>Information Id: ${item.id}</h1>
            <div>Title: ${item.title}</div>
            <div>Body: ${item.body}</div>
        `;
        $('#info').html(`${html}`);
    });
}
const onClickFilterData = () => {
    let html = '';
    infoData.filter((val)=>{
        return val.id > 5;
    }).map((item)=>{
        html += `
            <h1>Information Id: ${item.id}</h1>
            <div>Title: ${item.title}</div>
            <div>Body: ${item.body}</div>
        `;
        $('#info').html(`${html}`);
    });
}

const onClickFindData = () => {
    const op = infoData.find((item)=>{
        return item.id>5;
    });
    console.log(op);

    let html = `
            <h1>Information Id: ${op.id}</h1>
            <div>Title: ${op.title}</div>
            <div>Body: ${op.body}</div>
        `;
    $('#info').html(html);
    console.log(op.id);
}

const onClickFindIndexData = () => {
    const op = infoData.findIndex((item)=>{
        let lt = item.title;
        return lt.length>30;
    });
    console.log(op);

    console.log(infoData[op]);
    newTitle="Hey, I am new title";
    infoData[op].title=newTitle;
    console.log(infoData);
    let html = '<h1>Title is Changed.(To check please click on "All Information" Button and see Id 3s title.)</h1>';
    $('#info').html(html);

}