import React,{useState} from 'react'
import sideImg from '../../Assets/login.png'
import '../Login/Login.css';
const Login = (props) => {

    const {loggin}=props;

    const [userName, setuserName] = useState('');
    const [userNameErr, setuserNameErr] = useState('');
    const [pass, setpass] = useState('');
    const [passErr, setpassErr] = useState('');
    
   
    const onChangeUserName = (e) => {
        const pattern = /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/;
        if(pattern.test(e.target.value)){
            setuserName(e.target.value);
            setuserNameErr('');
        }
        else if(e.target.value===''){
            setuserNameErr('*Please, Enter Username');
        }
        else{
            setuserNameErr('*Sorry, Username is not valid.');
        }
    }
    const onchangePass =(e)=>{
        const pattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
        // const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
        if(pattern.test(e.target.value)){
            setpass(e.target.value);
            setpassErr('');
        }
        else if(e.target.value===''){
            setpassErr('*Please, Enter Password');
        }
        else{
            setpassErr('*Sorry, Password is not valid.');
        }
    }
    const onClickSignIn =(e)=>{
        if(userName===''){
            setuserNameErr('*Please, Enter Username');
        }
        if(pass===''){
            setpassErr('*Please, Enter Password');
        }
        e.preventDefault();
    }
    const onClickCreateAC =(e)=>{
        loggin();
        e.preventDefault();
    }
    return (
        <div className="main ">
            <div className="form">
                <div className="container-fluid d-flex justify-content-center my-min-high align-items-center">
                    <div className="row shadow rounded-3">
                        <div className="col-md-6 ">
                            <img src={sideImg} className="img-fluid h-100" alt="..."/>
                        </div>
                        <div className="col-md-6 ">
                            <div className="text-center my-2 fs-3 fw-bolder text-success">
                                Sign In
                            </div>
                            {/* <div className="social-icons d-flex justify-content-center my-3">
                                <div className="google-icon mx-2">
                                    <button className="my-btn-bg-remove">
                                        <img src={googleImg} alt="" height="20px" width="20px" />
                                    </button>
                                </div>
                                <div className="facebook-icon mx-2">
                                    <button className="my-btn-bg-remove">
                                        <img src={facebookImg} alt="" height="20px" width="20px" />
                                    </button>
                                </div>
                                <div className="twitter-icon mx-2">
                                    <button className="my-btn-bg-remove">
                                        <img src={twitterImg} alt="" height="20px" width="20px" />
                                    </button>
                                </div>
                                <div className="github-icon mx-2">
                                    <button className="my-btn-bg-remove">
                                        <img src={githubImg} alt="" height="20px" width="20px" />
                                    </button>
                                </div>
                            </div> */}
                            <div className="noteText text-center mt-4 mb-3 text-muted my-fs1 fw-lighter">
                                <a href="/" className="my-cutomise-a" onClick={onClickCreateAC}>create an account</a>
                            </div>
                            <form onSubmit={onClickSignIn}>
                                <div className="regfrom">
                                    <div className="email ">
                                        <input type="email" className="form-control my-fs1 my-outline" id="exampleFormControlInput2" placeholder="Username" onChange={onChangeUserName}/>
                                    </div>
                                    <div className="errMes">{userNameErr}</div>
                                    <div className="password ">
                                        <input type="password" className="form-control my-fs1" id="exampleFormControlInput3" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" onChange={onchangePass}/>
                                    </div>
                                    <div className="errMes">{passErr}</div>
                                </div>
                                <div className="tcp">
                                    <div className="d-flex justify-content-between">
                                        <div className="remember">
                                            <input className="form-check-input my-check-hover" type="checkbox" value="" id="flexCheckDefault" />
                                            <label className="ps-2 form-check-label my-fs align-center" htmlFor="flexCheckDefaul">
                                                Remember me
                                            </label>
                                        </div>
                                        <div className="forgotpass">
                                            <span className=""><a href="/" className="my-fs my-a">Forgot your password</a></span>
                                        </div>
                                    </div>
                                    <div className="errMes"></div>
                                </div>

                                <div className="btn-su-si  d-flex">
                                    
                                    <div className="btn-su w-100 ps-1">
                                        {/* <button type="button" className="btn btn-outline-success w-100" onClick={onClickSignIn}>Sign In</button> */}
                                        <input type="submit" className="btn btn-outline-success w-100"  value='Sign In'/>
                                    </div>
                                </div>
                            </form>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;
