// import React from 'react'

// function Tike(props) {
//     return (
//         <div>
//             <h2>Time Is : {props.data.toLocaleTimeString()}</h2>
//         </div>
//     )
// }
// // setInterval(Tike, 1000);

// export default Tike
import React, { Component } from 'react';

class Tike extends Component {
    constructor(props){
        super(props);
        this.state = {data: props.data};
    }

    componentDidMount=()=>{
        setInterval(() => {this.timer()}, 1000);
    }

    componentWillUnmount=()=>{
        clearInterval(()=>{this.timer()})
        //this is for LCmethods.js ->Remove click Button
        // alert("This clock will be removed.");
    }

    timer = () =>{
        this.setState({
            data: new Date()
        });
    }

    render() {
        return (
            <div>
                <h2>Time Is : {this.state.data.toLocaleTimeString()}</h2>
            </div>
        )
    }
}
export default Tike
