import React from 'react';

const Heading = (props) => {
    return (
        <div>
            <h1>Hello, {props.userObj.fname + ` ` + props.userObj.lname}  </h1>
        </div>
    )
}

export default Heading
