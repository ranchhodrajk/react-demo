import React, { Component } from 'react'
import Tike from './Tike';

class LCmethods extends Component {
    constructor(props){
        super(props);
        this.state = {
            fname: props.fname,
            lname: 'Doe',
            showHeading:true
        };
        console.log(`Hello, ${this.state.fname}`);
    }
    
    
    // static getDerivedStateFromProps(props,state){
    //     return {fname:props.fname}
    // }
    
    componentDidMount(){
        setTimeout(() => {
            this.setState({fname:"Darik"})
        }, 2000);
        console.log("Hello, componentDidMount");
    }

    shouldComponentUpdate() {
        return true;
    }

    getSnapshotBeforeUpdate(prevProps,prevState){

        if(prevProps!==this.props){
            console.log(`Before updating First Name:${prevState.fname}`);
            document.getElementById('before').innerHTML=`<p>Before updating First Name:${prevState.fname}</p>`
        }

        
        return null;
    }

    componentDidUpdate(){
        console.log(`After updating First Name:${this.state.fname}`);
        document.getElementById('after').innerHTML=`<p>After updating First Name:${this.state.fname}</p>`
    }

    onclickBtn=()=>{
        this.setState((state)=>({
            showHeading:false
        }));
    }

    // checkIt1(){
    //     alert("Auto Calling[With out binding] btn1");
    // }
    checkIt2(){
        alert("Calling on click     [With binding] btn2");
    }
    checkIt3(){
        alert("Calling on click     [With binding] btn3");
    }
    // checkIt4(){
    //     alert("Auto Calling     [With out binding] btn4");
    // }
    render() {
        console.log("Hello, render");
        
        return (
            <div>
                
                {this.state.showHeading===true?<Tike data={new Date()}/>:null}
                        
                <div className="first-name">First Name is: {this.state.fname}</div>
                <div className="last-name">Last Name is: {this.state.lname}</div>
                <br/>
                <div className="stopTike">
                    <button onClick={this.onclickBtn}>Remove Clock</button>
                    {/* <button onClick={this.checkIt1()}>Click me 1!</button> */}
                    <button onClick={()=>this.checkIt2()}>Click me 2!</button>
                    <button onClick={this.checkIt3.bind(this)}>Click me 3!</button>
                    {/* <button onClick={this.checkIt4(this)}>Click me 4!</button> */}
                </div>
                <div className="before" id="before"></div>
                <div className="after" id="after"></div>

            </div>
        )
    }
}

export default LCmethods
