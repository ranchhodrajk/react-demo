import React from 'react'

export const UserGreeting = () => {
    return (
        <div>
            Well-come back!
        </div>
    )
}
export const GuestGreeting = () => {
    return (
        <div>
            Please Sign up.
        </div>
    )
}
export const LoginButton = (props)=>{
    return(
        <div className="btn-Login">
            <button onClick={props.onClick}>Login</button>
        </div>
    )
}
export const LogoutButton = (props)=>{
    return(
        <div className="btn-Logout">
            <button onClick={props.onClick}>Logout</button>
        </div>
    )
}