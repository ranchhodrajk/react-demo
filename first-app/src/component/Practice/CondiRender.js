import React,{useState} from 'react'
import {UserGreeting,GuestGreeting,LoginButton,LogoutButton} from './UserGuest'
const CondiRender = (props) => {

    const [isOnOff, setisOnOff] = useState(props.isLoggin);
    const [isShow, setisShow] = useState(true);

    const onClickToggle=()=>{
        setisOnOff(!isOnOff);
    }
    
    const onclickShowHide=()=>{
        setisShow(!isShow);
    }

    // let showMes;
    // let showBtn;
    // if(isOnOff){
    //     showMes=<UserGreeting/>
    //     showBtn=<LogoutButton onClick={onClickToggle}/>
    // }
    // else{
    //     showMes=<GuestGreeting/>
    //     showBtn=<LoginButton onClick={onClickToggle}/>
    // }

    // return (
    //     <div>
    //         {showMes}
    //         {showBtn}
    //     </div>
    // )


    let showMes;
    let showBtn;
    if(isOnOff){
        showMes=<UserGreeting/>
        showBtn=<LogoutButton onClick={onClickToggle}/>
    }
    else{
        showMes=<GuestGreeting/>
        showBtn=<LoginButton onClick={onClickToggle}/>
    }

    return (
        <div>
            {
                isOnOff===true?
                    <div>
                        {
                            isShow?
                                <UserGreeting/>
                            :
                            null
                        }
                        <LogoutButton onClick={onClickToggle}/>
                    </div>
                :
                    <div>
                        {
                            isShow?
                                <GuestGreeting/>
                            :
                            null

                        }
                        <LoginButton onClick={onClickToggle}/>
                    </div>
            }
            <button onClick={onclickShowHide}>ShowHide</button>
        </div>
    )

}
export default CondiRender;