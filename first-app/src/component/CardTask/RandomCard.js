import React from "react";
import "./RandomCard.css";
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
const RandomCard = () => {
    const jsonData = [];


    //Generating rendom JSON

    for (let index = 0; index < 20; index++) {
        
        const fnameArr = ['Alvero','Jhon','Mark','Richard','Licaa','Rose'];
        const fnameRandom = Math.floor(Math.random() * fnameArr.length);

        const lnameArr = ['Moreno','Fered','Pitter','Echar','Archer','Warner'];
        const lnameRandom = Math.floor(Math.random() * lnameArr.length);

        const qualificationArr = ['Bsc','Msc','B.E','B.tech'];
        const qualificationRandom = Math.floor(Math.random() * qualificationArr.length);

        jsonData.push({
            id: index,
            title: "Employee",
            empDetail: {
                empProfile: `https://i.pravatar.cc/300?img=${Math.floor(Math.random() * 50) + 1}`,
                fname: `${fnameArr[fnameRandom]}`,
                lname: `${lnameArr[lnameRandom]}`,
                Qualification: `${qualificationArr[qualificationRandom]}`,
                experience: `${Math.floor(Math.random() * 10) + 1} Year`,
            },
        });
    }
    // const color = ['colorOrange','colorPink','colorBlue'];
    // const random = Math.floor(Math.random() * color.length);
    console.log(jsonData);
    return (
        <div className="main">
            <Navbar titleName={jsonData[0].title}/>
            <div className="container">
                <div className="row py-5">
                    <h1 className="text-dark">Employee details :</h1>
                </div>
                <div className="row">
                    {jsonData.map((item) => (
                        <div className="col-sm-3" key={Math.random()}>
                            <div className={`card bg-dark my-3  shadow rounded-3`}>
                                <div className="my-card-img rounded-circle">
                                    <img src={item.empDetail.empProfile} className="card-img-top rounded-circle" alt="..." height="80px" wedth="80px"/>
                                </div>
                                <div className="card-body text-center">
                                    <h5 className={`card-title text-center  `}>
                                        {item.empDetail.fname} {item.empDetail.lname}
                                    </h5>
                                    <div className="card-list text-muted">
                                        <div className="list">
                                            Heyy,
                                        </div>
                                        <div className="list">
                                            Qualification : {item.empDetail.Qualification}
                                        </div>
                                        <div className="list">
                                            Experience : {item.empDetail.experience}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <Footer titleName={jsonData[0].title}/>
        </div>
    );
};

export default RandomCard;
